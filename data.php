<?php 
$array = array (
	array (
	    'id' => '',
	    'text' => 'Miền Bắc',
	    'children' => [
    		array (
    	    	'id' => '{"code":"HAN","label":"H\\u00e0 N\\u1ed9i"}',
    	    	'text' => 'Hà Nội (HAN)',
    	  	),
    	  	array (
    	    	'id' => '{"code":"HPH","label":"H\\u1ea3i Ph\\u00f2ng"}',
    	    	'text' => 'Hải Phòng (HPH)',
    	  	),
    		array (
    	    	'id' => '{"code":"DIN","label":"\\u0110i\\u1ec7n Bi\\u00ean"}',
    	    	'text' => 'Điện Biên (DIN)',
    	  	), 
    	 	array (
    		    'id' => '{"code":"VDO","label":"V\\u00e2n \\u0110\\u1ed3n"}',
    		    'text' => 'Vân Đồn (VDO)',
    	  	),
	    ],
	 ),
  	array (
    	'id' => '',
    	'text' => 'Miền Trung',
    	'children' => [
		  	array (
		    	'id' => '{"code":"PXU","label":"Pleiku"}',
		    	'text' => 'Pleiku (PXU)',
		  	),
			array (
			    'id' => '{"code":"VCL","label":"Chu Lai"}',
			    'text' => 'Chu Lai (VCL)',
		  	),
		  	array (
		    	'id' => '{"code":"TBB","label":"Tuy H\\u00f2a"}',
		    	'text' => 'Tuy Hòa (TBB)',
		  	),
			array (
		    	'id' => '{"code":"VDH","label":"\\u0110\\u1ed3ng H\\u1edbi"}',
		    	'text' => 'Đồng Hới (VDH)',
		  	),
			array (
		    	'id' => '{"code":"HUI","label":"Hu\\u1ebf"}',
		    	'text' => 'Huế (HUI)',
		  	),
			array (
		    	'id' => '{"code":"DAD","label":"\\u0110\\u00e0 N\\u1eb5ng"}',
		    	'text' => 'Đà Nẵng (DAD)',
		  	),
			array (
		    	'id' => '{"code":"VII","label":"Vinh "}',
		    	'text' => 'Vinh  (VII)',
		  	),
			array (
		    	'id' => '{"code":"THD","label":"Thanh h\\u00f3a"}',
		    	'text' => 'Thanh hóa (THD)',
		 	),
    	],
  	), 
  	
	array (
	    'id' => '',
	    'text' => 'Miền Nam',
	    'children' => [
	    	array (
			    'id' => '{"code":"UIH","label":"Quy Nh\\u01a1n"}',
			    'text' => 'Quy Nhơn (UIH)',
		  	),
			array (
			    'id' => '{"code":"BMV","label":"Bu\\u00f4n Ma Thu\\u1ed9t"}',
			    'text' => 'Buôn Ma Thuột (BMV)',
		 	),
			array (
			    'id' => '{"code":"CAH","label":"C\\u00e0 Mau"}',
			    'text' => 'Cà Mau (CAH)',
		  	),
		  	array (
			    'id' => '{"code":"VKG","label":"R\\u1ea1ch Gi\\u00e1"}',
			    'text' => 'Rạch Giá (VKG)',
		  	),
			array (
			    'id' => '{"code":"VCS","label":"C\\u00f4n \\u0110\\u1ea3o"}',
			    'text' => 'Côn Đảo (VCS)',
		  	),
			array (
			    'id' => '{"code":"VCA","label":"C\\u1ea7n Th\\u01a1"}',
			    'text' => 'Cần Thơ (VCA)',
		  	),
			array (
			    'id' => '{"code":"PQC","label":"Ph\\u00fa Qu\\u1ed1c"}',
			    'text' => 'Phú Quốc (PQC)',
		  	),
			array (
			    'id' => '{"code":"DLI","label":"\\u0110\\u00e0 L\\u1ea1t"}',
			    'text' => 'Đà Lạt (DLI)',
		  	),
			array (
			    'id' => '{"code":"CXR","label":"Nha Trang"}',
			    'text' => 'Nha Trang (CXR)',
		  	),
			array (
			    'id' => '{"code":"SGN","label":"H\\u1ed3 Ch\\u00ed Minh"}',
			    'text' => 'Hồ Chí Minh (SGN)',
		  	),
	    ],
  	),
	
	array (
	    'id' => '',
	    'text' => 'Đông Nam Á',
	    'children' => [
    		array (
    		    'id' => '{"code":"MNL","label":"Manila "}',
    		    'text' => 'Manila  (MNL)',
    	  	),
    		array (
    		    'id' => '{"code":"KUL","label":"Kuala Lumpur "}',
    		    'text' => 'Kuala Lumpur  (KUL)',
    	  	),
    		array (
    		    'id' => '{"code":"RGN","label":"Yangon "}',
    		    'text' => 'Yangon  (RGN)',
    	  	),
    		array (
    		    'id' => '{"code":"VTE","label":"Vientiane "}',
    		    'text' => 'Vientiane  (VTE)',
    	  	),
    		array (
    		    'id' => '{"code":"CGK","label":"Jakarta "}',
    		    'text' => 'Jakarta  (CGK)',
    	  	),
    		array (
    		    'id' => '{"code":"PKZ","label":"Pakse "}',
    		    'text' => 'Pakse  (PKZ)',
    	  	),
    		array (
    		    'id' => '{"code":"REP","label":"Siem Reap "}',
    		    'text' => 'Siem Reap  (REP)',
    	  	),
    		array (
    		    'id' => '{"code":"PNH","label":"Phnom Penh"}',
    		    'text' => 'Phnom Penh (PNH)',
    	  	),
    		array (
    		    'id' => '{"code":"BKK","label":"Bangkok"}',
    		    'text' => 'Bangkok (BKK)',
    		),
    		array (
    		    'id' => '{"code":"SIN","label":"Singapore"}',
    		    'text' => 'Singapore (SIN)',
    	  	),
	    ],
  	),
	
	array (
	    'id' => '',
	    'text' => 'Trung Quốc',
	    'children' => [
    		array (
    		    'id' => '{"code":"MFM","label":"Macau "}',
    		    'text' => 'Macau  (MFM)',
    	  	),
    		array (
    		    'id' => '{"code":"HKG","label":"Hong Kong "}',
    		    'text' => 'Hong Kong  (HKG)',
    	  	),
	    ],
 	 ),
	
	array (
	    'id' => '',
	    'text' => 'Bắc Á',
	    'children' => [
    		array (
    		    'id' => '{"code":"TPE","label":"\\u0110\\u00e0i B\\u1eafc "}',
    		    'text' => 'Đài Bắc  (TPE)',
    	  	),
    		array (
    		    'id' => '{"code":"KHH","label":"Cao H\\u00f9ng "}',
    		    'text' => 'Cao Hùng  (KHH)',
    	  	),
    		array (
    		    'id' => '{"code":"ICN","label":"Seoul "}',
    		    'text' => 'Seoul  (ICN)',
    	  	),
    		array (
    		    'id' => '{"code":"GMP","label":"Seoul "}',
    		    'text' => 'Seoul  (GMP)',
    	  	),
    		array (
    		    'id' => '{"code":"PUS","label":"Busan "}',
    		    'text' => 'Busan  (PUS)',
    	  	),
    		array (
    		    'id' => '{"code":"NRT","label":"Tokyo "}',
    		    'text' => 'Tokyo  (NRT)',
    	  	),
    		array (
    		    'id' => '{"code":"KIX","label":"Osaka "}',
    		    'text' => 'Osaka  (KIX)',
    	  	),
    		array (
    		    'id' => '{"code":"NGO","label":"Nagoya "}',
    		    'text' => 'Nagoya  (NGO)',
    	  	),
    		array (
    		    'id' => '{"code":"TRV","label":"Trivandrum "}',
    		    'text' => 'Trivandrum  (TRV)',
    	  	),
    		array (
    		    'id' => '{"code":"CAN","label":"Qu\\u1ea3ng Ch\\u00e2u "}',
    		    'text' => 'Quảng Châu  (CAN)',
    	  	),
    		array (
    		    'id' => '{"code":"PEK","label":"B\\u1eafc Kinh "}',
    		    'text' => 'Bắc Kinh  (PEK)',
    	  	),
    		array (
    		    'id' => '{"code":"CSX","label":"Changsha "}',
    		    'text' => 'Changsha  (CSX)',
    	  	),
    		array (
    		    'id' => '{"code":"SHA","label":"Th\\u01b0\\u1ee3ng H\\u1ea3i "}',
    		    'text' => 'Thượng Hải  (SHA)',
    	  	),
    		array (
    		    'id' => '{"code":"NNG","label":"Nam Ninh "}',
    		    'text' => 'Nam Ninh  (NNG)',
    	  	),
    		array (
    		    'id' => '{"code":"PVG","label":"Th\\u01b0\\u1ee3ng H\\u1ea3i "}',
    		    'text' => 'Thượng Hải  (PVG)',
    	  	),
    		array (
    		    'id' => '{"code":"HYD","label":"Hyderabad "}',
    		    'text' => 'Hyderabad  (HYD)',
    	  	),
	    ],
 	),
	
	array (
	    'id' => '',
	    'text' => 'Châu Âu',
	    'children' => [
    		array (
    		    'id' => '{"code":"LHR","label":"London "}',
    		    'text' => 'London  (LHR)',
    		 ),
    		array (
    		    'id' => '{"code":"MOW","label":"Moscow "}',
    		    'text' => 'Moscow  (MOW)',
    	  	),
    		array (
    		    'id' => '{"code":"FRA","label":"Frankfurt "}',
    		    'text' => 'Frankfurt  (FRA)',
    	  	),
    		array (
    		    'id' => '{"code":"CDG","label":"Paris "}',
    		    'text' => 'Paris  (CDG)',
    	  	),
    		array (
    		    'id' => '{"code":"PRG","label":"Prague "}',
    		    'text' => 'Prague  (PRG)',
    	  	),
	    ],
  	),
	
	array (
	    'id' => '',
	    'text' => 'Châu Úc',
	    'children' => [
    		array (
    		    'id' => '{"code":"DRW","label":"Darwin "}',
    		    'text' => 'Darwin  (DRW)',
    	  	),
    		array (
    		    'id' => '{"code":"SYD","label":"Sydney "}',
    		    'text' => 'Sydney  (SYD)',
    	  	),
    		array (
    		    'id' => '{"code":"MEL","label":"Melbourne "}',
    		    'text' => 'Melbourne  (MEL)',
    	  	),
    		array (
    		    'id' => '{"code":"AKL","label":"Auckland "}',
    		    'text' => 'Auckland  (AKL)',
    	  	),
	    ],
  	),
	
	array (
	    'id' => '',
	    'text' => 'Châu Mỹ',
	    'children' => [
    		array (
    		    'id' => '{"code":"SFO","label":"San Francisco "}',
    		    'text' => 'San Francisco  (SFO)',
    	  	),
    		array (
    		    'id' => '{"code":"BOS","label":"Boston "}',
    		    'text' => 'Boston  (BOS)',
    	  	),
    		array (
    		    'id' => '{"code":"YTO","label":"Toronto "}',
    		    'text' => 'Toronto  (YTO)',
    	  	),
    		array (
    		    'id' => '{"code":"SEA","label":"Seattle, WA "}',
    		    'text' => 'Seattle, WA  (SEA)',
    	  	),
    		array (
    		    'id' => '{"code":"MSP","label":"Minneapolis "}',
    		    'text' => 'Minneapolis  (MSP)',
    	  	),
    		array (
    		    'id' => '{"code":"CHI","label":"Chicago "}',
    		    'text' => 'Chicago  (CHI)',
    	  	),
    		array (
    		    'id' => '{"code":"LAX","label":"Los Angeles "}',
    		    'text' => 'Los Angeles  (LAX)',
    	  	),
	    ],
  	),
	
);
echo json_encode($array);exit();

?>