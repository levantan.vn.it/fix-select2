<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Select2</title>
<link href="select2.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="select2.js"></script>
<style type="text/css">
	.select2-drop-active{
		width: 1024px!important
	}
	.select2-results-dept-0{
		width: calc(100% / 9);
		float: left;
	}
	.select2-results-dept-0 > .select2-result-label{
		color: #ff9406!important;
	}
	.select2-results-dept-0 .select2-result-sub .select2-result-label{
		font-size: 12px!important;
		padding-left: 5px!important;
	}
	.select2-results{
		max-height: initial
	}
</style>
</head>

<body>
<input type="text" id="searching" class="locationlist" style="width: 400px">
<input type="hidden" name="hotelcity" id="locationid" required="" value="">
<script type="text/javascript">
	$('#searching').select2({
	  ajax: {
	    url: 'http://select2.on/data.php',
	    dataType: 'json',
	    results: function (data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          return {
            results: data
          };
        }
	    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
	  },
	  formatResult: topicFormatResult,
	   formatSelection: formatRepoSelection,
	});
	function topicFormatResult(data) {
	    return data.text
	  }
	  function formatRepoSelection(data) {
	  	var id = jQuery.parseJSON( data.id );
	    return '<option value="'+ id.code +'">' + data.text + '</option>'
	  }
</script>
</body>
</html>